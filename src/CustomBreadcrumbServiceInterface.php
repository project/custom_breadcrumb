<?php

namespace Drupal\custom_breadcrumb;

/**
 * Interface CustomBreadcrumbServiceInterface.
 */
interface CustomBreadcrumbServiceInterface {

  /**
   * Default breadcrumb separator.
   */
  const CB_SEPARATOR = '/';

  /**
   * Information storage way (like: node, view, route or other/url).
   */
  const CB_TYPE = 'other';

  /**
   * Main table of the custom_breadcrumb module.
   */
  const CB_MAIN = 'custom_breadcrumb_main';

  /**
   * Table to store breadcrumb sets.
   */
  const CB_SETS = 'custom_breadcrumb_sets';

  /**
   * Check and confirm if the both tables of custom_breadcrumb module exists.
   *
   * @return bool
   *   True on table exists, otherwise FALSE and log error.
   */
  public function checkTableExists();

  /**
   * Check if url string given is valid or not.
   *
   * @param string $url
   *   A string of url to validate.
   * @param bool $want_route
   *   TRUE/FALSE whether you want route or not.
   *
   * @return mixed
   *   $want_route = TRUE, route name is returned, otherwise bool(TRUE/FALSE).
   */
  public function checkUrlExists(string $url, $want_route = FALSE);

  public function insertUpdate(array $info, $is_update = FALSE);

}

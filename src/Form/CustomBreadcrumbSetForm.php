<?php

namespace Drupal\custom_breadcrumb\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\custom_breadcrumb\CustomBreadcrumbServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create new breadcrumb sets for a page.
 */
class CustomBreadcrumbSetForm extends FormBase {

  /**
   * Drupal\custom_breadcrumb\CustomBreadcrumbServiceInterface definition.
   *
   * @var \Drupal\custom_breadcrumb\CustomBreadcrumbServiceInterface
   */
  protected $customBreadcrumb;

  public function __construct(CustomBreadcrumbServiceInterface $custom_breadcrumb) {
    $this->customBreadcrumb = $custom_breadcrumb;
  }

  public static function create(ContainerInterface $container) {
    return new static(
    // Load the service required to construct this class.
      $container->get('custom_breadcrumb.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_breadcrumb_set_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $num_rows = $form_state->get('num_rows');

    $form['breadcrumb_page'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page url to set breadcrumb'),
      '#title_display' => 'invisible',
      '#description' => $this->t('Page Url where to set below sets of breadcrumb.'),
      '#required' => TRUE,
    ];

    // Breadcrumb sets wrapper start.
    $form['breadcrumb_wrapper'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Breadcrumb sets'),
      '#prefix' => '<div id="breadcrumb-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['breadcrumb_wrapper']['actions']['#type'] = 'actions';

    // Home page in the breadcrumb sets.
    $form['breadcrumb_wrapper']['home'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Home'),
      '#default_value' => TRUE,
    ];

    $form['breadcrumb_wrapper']['home_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default text'),
      '#title_display' => 'invisible',
      '#description' => $this->t('Text will be used to display Home page link in the breadcrumb sets.'),
      '#default_value' => $this->t('Home'),
      '#states' => [
        'visible' => [
          ':input[name="breadcrumb_wrapper[home]"]' => ['checked' => TRUE],
        ],
        'enabled' => [
          ':input[name="breadcrumb_wrapper[home]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="breadcrumb_wrapper[home]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Rest dynamic Breadcrumb sets.
    if (empty($num_rows)) {
      $range = range(0, 0);
      $form_state->set('num_rows', $range);
      $num_rows = $form_state->get('num_rows');
    }

    foreach ($num_rows as $delta) {
      $form['breadcrumb_wrapper']['breadcrumb_set'][$delta] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['container-inline'],
        ],
      ];

      $form['breadcrumb_wrapper']['breadcrumb_set'][$delta]['breadcrumb_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Text'),
        '#required' => TRUE,
      ];

      $form['breadcrumb_wrapper']['breadcrumb_set'][$delta]['breadcrumb_url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Url'),
        '#required' => TRUE,
      ];

      // Remove button to remove each set.
      if (count($num_rows) > 1) {
        $form['breadcrumb_wrapper']['breadcrumb_set'][$delta]['remove_set'] = [
          '#type' => 'submit',
          '#name' => 'remove_set_' . $delta,
          '#value' => $this->t('Remove'),
          '#submit' => ['::removeOneSet'],
          '#limit_validation_errors' => [],
          '#ajax' => [
            'callback' => '::neutralCallback',
            'wrapper' => 'breadcrumb-wrapper',
          ],
        ];
      }
    }

    // Add button to increase breadcrumb sets.
    $form['breadcrumb_wrapper']['actions']['add_set'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add More'),
      '#submit' => ['::addOneSet'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::neutralCallback',
        'wrapper' => 'breadcrumb-wrapper',
      ],
    ];

    // Preview button to see the breadcrumbs till set.
    $form['breadcrumb_wrapper']['actions']['preview_wrapper']['preview'] = [
      '#type' => 'submit',
      '#value' => $this->t('Preview breadcrumb'),
      '#ajax' => [
        'callback' => '::showPreview',
        'wrapper' => 'custom-breadcrumb-preview',
      ],
      '#suffix' => '<div id="custom-breadcrumb-preview"></div>',
    ];

    // Submit actions.
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['button button--primary'],
      ],
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * Add more functionality, to add one more breadcrumb set.
   */
  public function addOneSet(array &$form, FormStateInterface $form_state) {
    $form_state->get('num_rows')[] = count($form_state->get('num_rows')) > 0 ? max($form_state->get('num_rows')) + 1 : 0;
    $form_state->setRebuild();
  }

  /**
   * Remove functionality, to remove particular from breadcrumb sets.
   */
  public function removeOneSet(array &$form, FormStateInterface $form_state) {
    $delta_remove = $form_state->getTriggeringElement()['#parents'][2];
    $k = array_search($delta_remove, $form_state->get('num_rows'));
    unset($form_state->get('num_rows')[$k]);
    $form_state->setRebuild();
  }

  /**
   * Callback to return breadcrumb wrapper after, add or remove functionality.
   */
  public function neutralCallback(array &$form, FormStateInterface $form_state) {
    return $form['breadcrumb_wrapper'];
  }

  /**
   * Show preview of the breadcrumb.
   */
  public function showPreview(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $markup = '';
    $breadcrumbs = [];
    if (isset($values['breadcrumb_wrapper']['home']) && $values['breadcrumb_wrapper']['home'] == TRUE) {
      global $base_path;
      $text = $values['breadcrumb_wrapper']['home_text'];
      $url = Url::fromUserInput($base_path);
      $breadcrumbs[] = Link::fromTextAndUrl($text, $url)->toString();
    }
    if (isset($values['breadcrumb_wrapper']['breadcrumb_set'])) {
      foreach ($values['breadcrumb_wrapper']['breadcrumb_set'] as $bread) {
        if (!empty($bread['breadcrumb_text']) && !empty($bread['breadcrumb_url'])) {
          $url = Url::fromUserInput($bread['breadcrumb_url']);
          $breadcrumbs[] = Link::fromTextAndUrl($bread['breadcrumb_text'], $url)
            ->toString();
        }
      }
    }

    if (!empty($breadcrumbs)) {
      $markup = implode($this->customBreadcrumb::CB_SEPARATOR, $breadcrumbs);
    }

    $ajaxresponse = new AjaxResponse();
    $ajaxresponse->addCommand(new HtmlCommand('#custom-breadcrumb-preview', $markup));
    return $ajaxresponse;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $info = [
      'breadcrumb_page' => $values['breadcrumb_page'],
      'route' => $this->customBreadcrumb->checkUrlExists($values['breadcrumb_page'], TRUE),
      'is_home' => isset($values['breadcrumb_wrapper']['home']) ? $values['breadcrumb_wrapper']['home'] : 0,
      'sets' => isset($values['breadcrumb_wrapper']['breadcrumb_set']) ? $values['breadcrumb_wrapper']['breadcrumb_set'] : '',
    ];
    $this->customBreadcrumb->insertUpdate($info);
    $replace = Link::fromTextAndUrl($values['breadcrumb_page'], Url::fromUserInput($values['breadcrumb_page']))->toString();
    $this->messenger()
      ->addStatus($this->t('New breadcrumb has been set to @page.', ['@page' => $replace]));
    $form_state->setRedirect('custom_breadcrumb.custom_breadcrumb_default_form');
  }

}

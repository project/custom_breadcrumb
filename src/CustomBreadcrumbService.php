<?php

namespace Drupal\custom_breadcrumb;

use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Database\Driver\mysql\Connection;
use Psr\Log\LoggerInterface;
use Drupal\Core\StringTranslation\TranslationManager;

/**
 * Class CustomBreadcrumbService.
 */
class CustomBreadcrumbService implements CustomBreadcrumbServiceInterface {

  /**
   * Drupal\Core\Path\PathValidatorInterface definition.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Psr\Log\LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * Constructs a new CustomBreadcrumbService object.
   */
  public function __construct(PathValidatorInterface $path_validator, Connection $database, LoggerInterface $logger, TranslationManager $string_translation) {
    $this->pathValidator = $path_validator;
    $this->database = $database;
    $this->logger = $logger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function checkTableExists() {
    if ($this->database->schema()
        ->tableExists(static::CB_MAIN) && $this->database->schema()
        ->tableExists(static::CB_SETS)) {
      return TRUE;
    }
    $this->logger->error($this->stringTranslation->translate('Unable to perform any read/write operation, because ' . static::CB_MAIN . ' and ' . static::CB_SETS . ' table does not exists.'));
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUrlExists(string $url, $want_route = FALSE) {
    if ($check = $this->pathValidator->getUrlIfValid($url)) {
      if ($want_route == TRUE) {
        return $check->getRouteName();
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function insertUpdate(array $info, $is_update = FALSE) {
    if ($this->checkTableExists() == TRUE) {
      try {
        $query = $this->database->insert(static::CB_MAIN);
        $query->values([
          'type' => static::CB_TYPE,
          'breadcrumb_page' => $info['breadcrumb_page'],
          'route' => $info['route'],
          'is_home' => $info['home'],
        ]);
        $query->execute();
      }
      catch (\Exception $exception) {
        $this->logger->debug($this->stringTranslation->translate('An unexpected error occurred while inserting data'), $exception);
      }
    }
    return FALSE;
  }

}
